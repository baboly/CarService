package com.babaktamjidi.carservice.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

public class ServiceDTO {

    private Integer id;

    @Temporal(TemporalType.DATE)
    private Date date;

    @Temporal(TemporalType.DATE)
    private Date date1;

    @NotBlank
    private String note;

    private Integer carID;

    private Integer mechanicID;

    @NotBlank
    private String carRegNr;

    public Date getDate1() {
        return date1;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public String getCarRegNr() {
        return carRegNr;
    }

    public void setCarRegNr(String carRegNr) {
        this.carRegNr = carRegNr;
    }

    public Integer getMechanicID() {
        return mechanicID;
    }

    public void setMechanicID(Integer mechanicID) {
        this.mechanicID = mechanicID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setDate(Date date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getCarID() {
        return carID;
    }

    public void setCarID(Integer carID) {
        this.carID = carID;
    }

    @Override
    public String toString() {
        return "ServiceDTO [id=" + id + ", date=" + date + ", note=" + note
                + ", carID=" + carID + ", mechanicID=" + mechanicID
                + ", carRegNr=" + carRegNr + "]";
    }

    public void setService(Date date, String note, Integer carID, Integer mechanicID) {
        setDate(date);
        setNote(note);
        setMechanicID(mechanicID);
        setCarID(carID);
    }
}
