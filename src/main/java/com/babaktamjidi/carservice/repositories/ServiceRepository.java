package com.babaktamjidi.carservice.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.babaktamjidi.carservice.entities.Car;
import com.babaktamjidi.carservice.entities.Service;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Integer> {

    Service findByDate(Date date);

    List<Car> findByCar(Car car);

    List<Service> findByCarId(Integer carId);

    @Query("select count(c) from Service c")
    Integer countTotalServices();

    @Query("SELECT regnr FROM Car c WHERE c.regnr = :regnr")
    String getCar(@Param("regnr") String string);

    @Query("SELECT Count(c) from Service c WHERE c.mechanicID = :mechanicid")
    Integer CountByBrand(@Param("mechanicid") Integer mechanicid);

    @Query("SELECT Count(c) from Service c WHERE (c.date BETWEEN :date1 AND :date2)")
    Integer CountBetweenDates(@Param("date1") Date date1, @Param("date2") Date date2);

    @Query("SELECT Count(c) from Service c WHERE (c.date BETWEEN '' AND :date2)")
    Integer CountServiceJanuary();

    @Query("SELECT COUNT(1) FROM Service WHERE MONTH(service_Date) = 1 AND YEAR(service_date) = YEAR(NOW())")
    Integer CountServicesForJan();

    @Query("SELECT COUNT(1) FROM Service WHERE MONTH(service_Date) = 2 AND YEAR(service_date) = YEAR(NOW())")
    Integer CountServicesForFeb();

    @Query("SELECT COUNT(1) FROM Service WHERE MONTH(service_Date) = 3 AND YEAR(service_date) = YEAR(NOW())")
    Integer CountServicesForMar();

    @Query("SELECT COUNT(1) FROM Service WHERE MONTH(service_Date) = 4 AND YEAR(service_date) = YEAR(NOW())")
    Integer CountServicesForApr();

    @Query("SELECT COUNT(1) FROM Service WHERE MONTH(service_Date) = 5 AND YEAR(service_date) = YEAR(NOW())")
    Integer CountServicesForMaj();

    @Query("SELECT COUNT(1) FROM Service WHERE MONTH(service_Date) = 6 AND YEAR(service_date) = YEAR(NOW())")
    Integer CountServicesForJun();

    @Query("SELECT COUNT(1) FROM Service WHERE MONTH(service_Date) = 7 AND YEAR(service_date) = YEAR(NOW())")
    Integer CountServicesForJul();

    @Query("SELECT COUNT(1) FROM Service WHERE MONTH(service_Date) = 8 AND YEAR(service_date) = YEAR(NOW())")
    Integer CountServicesForAug();

    @Query("SELECT COUNT(1) FROM Service WHERE MONTH(service_Date) = 9 AND YEAR(service_date) = YEAR(NOW())")
    Integer CountServicesForSep();

    @Query("SELECT COUNT(1) FROM Service WHERE MONTH(service_Date) = 10 AND YEAR(service_date) = YEAR(NOW())")
    Integer CountServicesForOkt();

    @Query("SELECT COUNT(1) FROM Service WHERE MONTH(service_Date) = 11 AND YEAR(service_date) = YEAR(NOW())")
    Integer CountServicesForNov();

    @Query("SELECT COUNT(1) FROM Service WHERE MONTH(service_Date) = 12 AND YEAR(service_date) = YEAR(NOW())")
    Integer CountServicesForDec();
}
