package com.babaktamjidi.carservice.services.interfaces;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;

import com.babaktamjidi.carservice.dto.ServiceDTO;
import com.babaktamjidi.carservice.entities.Service;

public interface ServiceService {

	List<Service> getAll();

	void create(ServiceDTO service);
	
	List<Service> findByCarId(Integer id);
	
	void delete(Integer id);

	Integer countTotalServices();
	
	List<Integer> CountByBrand();
	
	Integer CountBetweenDates(@Param("servicedate1") Date date1, @Param("servicedate2") Date date2 );
	
	List<Integer> CountServicesYearReport();
	
}
