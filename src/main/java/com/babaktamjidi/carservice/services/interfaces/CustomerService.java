package com.babaktamjidi.carservice.services.interfaces;

import java.util.List;

import com.babaktamjidi.carservice.dto.CustomerDTO;
import com.babaktamjidi.carservice.entities.Customer;

public interface CustomerService {

	void create(CustomerDTO newCustomer);
		
	List<Customer> getAll();
	
	Customer findBySSN(String ssn);
	
	Customer update(CustomerDTO updated);
	
	void delete(CustomerDTO customer);

	Integer countTotalCustomers();
	
	Integer countServiceForFemales();
	
	Integer countServiceForMales();

	List<Integer> countServiceByGender();
}
