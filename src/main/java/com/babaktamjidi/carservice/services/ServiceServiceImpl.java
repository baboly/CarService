package com.babaktamjidi.carservice.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.babaktamjidi.carservice.services.interfaces.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.babaktamjidi.carservice.dto.ServiceDTO;
import com.babaktamjidi.carservice.entities.Car;
import com.babaktamjidi.carservice.entities.Service;
import com.babaktamjidi.carservice.repositories.CarRepository;
import com.babaktamjidi.carservice.repositories.ServiceRepository;

@Component
public class ServiceServiceImpl implements ServiceService {

    private ServiceRepository serviceRepository;
    private CarRepository carRepository;

    @Autowired
    public ServiceServiceImpl(ServiceRepository serviceRepository,
                               CarRepository carRepository) {
        this.serviceRepository = serviceRepository;
        this.carRepository = carRepository;
    }

    @Transactional
    @Override
    public List<Service> getAll() {
        return serviceRepository.findAll();
    }

    @Transactional
    @Override
    public void create(ServiceDTO newService) {

        Service service = new Service();
        Car car = carRepository.findByRegnr(newService.getCarRegNr());
        String brand = car.getBrand();

        switch (brand) {
            case "Volvo": {
                service.setService(newService.getDate(), newService.getNote(), car.getId(), 1);
                break;
            }
            case "BMW": {
                service.setService(newService.getDate(), newService.getNote(), car.getId(), 2);
                break;
            }
            case "Audi": {
                service.setService(newService.getDate(), newService.getNote(), car.getId(), 3);
                break;
            }
            case "Mercedes": {
                service.setService(newService.getDate(), newService.getNote(), car.getId(), 4);
                break;
            }
            case "Volkswagen": {
                service.setService(newService.getDate(), newService.getNote(), car.getId(), 5);
                break;
            }
            default: {
                service.setService(newService.getDate(), newService.getNote(), car.getId(), 6);
            }
        }
        serviceRepository.save(service);
    }

    @Override
    public List<Service> findByCarId(Integer id) {
        return serviceRepository.findByCarId(id);
    }

    @Override
    public void delete(Integer id) {
        serviceRepository.delete(id);
    }

    @Override
    public Integer countTotalServices() {
        return serviceRepository.countTotalServices();
    }

    @Override
    public List<Integer> CountByBrand() {
        Integer volvo;
        Integer bmw;
        Integer audi;
        Integer mercedes;
        Integer volkswagen;
        List<Integer> counts = new ArrayList<>();

        volvo = serviceRepository.CountByBrand(1);
        bmw = serviceRepository.CountByBrand(2);
        audi = serviceRepository.CountByBrand(3);
        mercedes = serviceRepository.CountByBrand(4);
        volkswagen = serviceRepository.CountByBrand(5);

        counts.add(0, volvo);
        counts.add(1, bmw);
        counts.add(2, audi);
        counts.add(3, mercedes);
        counts.add(4, volkswagen);
        return counts;
    }

    @Override
    public Integer CountBetweenDates(Date date1, Date date2) {
        return serviceRepository.CountBetweenDates(date1, date2);
    }

    @Transactional
    @Override
    public List<Integer> CountServicesYearReport() {
        List<Integer> yearReport = new ArrayList<>();
        yearReport.add(0, serviceRepository.CountServicesForJan());
        yearReport.add(1, serviceRepository.CountServicesForFeb());
        yearReport.add(2, serviceRepository.CountServicesForMar());
        yearReport.add(3, serviceRepository.CountServicesForApr());
        yearReport.add(4, serviceRepository.CountServicesForMaj());
        yearReport.add(5, serviceRepository.CountServicesForJun());
        yearReport.add(6, serviceRepository.CountServicesForJul());
        yearReport.add(7, serviceRepository.CountServicesForAug());
        yearReport.add(8, serviceRepository.CountServicesForSep());
        yearReport.add(9, serviceRepository.CountServicesForOkt());
        yearReport.add(10, serviceRepository.CountServicesForNov());
        yearReport.add(11, serviceRepository.CountServicesForDec());
        return yearReport;
    }
}
